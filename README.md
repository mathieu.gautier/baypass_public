----------------------

[TOC]

----------------------

# Overview

*BayPass* is a population genomics software which is primarily designed to identify genetic markers subjected to selection and/or associated with population-specific covariates (e.g., environmental variables, quantitative or categorical phenotypic traits). The underlying models explicitly account for (and can estimate) the covariance structure among the population allele frequencies resulting from the shared history of the populations under study. The [manual](manual/BayPass_manual.pdf) provides information about the models, about how to format the data file, how to specify the user-defined parameters, and how to interpret the results. Some utilities are included in the *utils* directory of the package to facilitate post-processing of the output or perform complementary analyses.

# Get *BayPass*

The current version of *BayPass* can be directly downloaded using the [Download](https://forgemia.inra.fr/mathieu.gautier/baypass_public) button on top of this overview page or cloned using addresses provided on the [Clone](https://forgemia.inra.fr/mathieu.gautier/baypass_public) button

To view archives for all releases since Version 2.1:

- on the left sidebar, select Deployments > Releases, or
- click on the above [Release](https://forgemia.inra.fr/mathieu.gautier/baypass_public) button indicating the number of releases

# Requirements

## A modern Fortran compiler

*BayPass* is coded in modern Fortran and can therefore be compiled for any system supporting a Fortran 90 (or higher) compiler. More specifically, the provided Makefile  is designed to work with:

- the free compiler *gfortran* (version >=7.0 recommended) or;
- the commercial Intel® Fortran compiler *ifort* which is available at no cost for most platforms as
part of the [Intel® oneAPI HPC Toolkit](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html).

Note also that *BayPass* relies on [*OpenMP*](https://www.openmp.org/wp/) to implement multi-threading (usually installed on most Linux OS). 

With *gfortran* or *ifort*, compiling the source can simply be done with the following `make` instructions run within the *source* sub-directory:

-  with *gfortran* (the command then produces an executable called `g_baypass`):
``` 
make clean all FC=gfortran
```

- with *ifort* (the command then produces an executable called `i_baypass`):
``` 
make clean all FC=ifort
```
See the [manual](manual/BayPass_manual.pdf) for more details on how to compile

## The R software (optional but recommended)

The functions available in the [utils](utils) directories require a recent version of [R](https://cran.r-project.org/) (e.g. >=3.0) and the packages [*mvtnorm*](https://cran.r-project.org/web/packages/mvtnorm/index.html) and [*geigen*](https://cran.r-project.org/web/packages/geigen/index.html). Although not needed, the packages [*poolfstat*](https://cran.r-project.org/web/packages/poolfstat/index.html) include several function that may be useful to prepare input files.

# See the [manual](manual/BayPass_manual.pdf) for more details.

# Licensing

See the [LICENSE](./LICENSE) file for licensing information as it pertains to all files in this repository.

# Reference

[Gautier (2015)](https://doi.org/10.1534/genetics.115.181453). Genome-Wide Scan for Adaptive Differentiation and Association Analysis with population-specific covariables. *Genetics*, 201(4):1555-1579.

[Olazcuaga *et al.* (2020)](https://doi.org/10.1093/molbev/msaa098). A whole-genome scan for association with invasion success in the fruit fly Drosophila suzukii using contrasts of allele frequencies corrected for population structure. *Molecular Biology and Evolution*, 37(8):2369-2385.

